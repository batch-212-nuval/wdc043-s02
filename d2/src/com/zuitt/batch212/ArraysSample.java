package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args) {

//  Arrays - Fixed / Limited Collection of data
//  2^31 = 2,147,483,648 elements

//  Declaration of Array
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

//  Other Syntax used to declare Array

        int intSample[] = new int[2];

        intSample[0] = 50;

        System.out.println(intSample[0]);

//  String Array

        String stringArray[] = new String[3];

        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Joe";

//  Declaration with Initialization

        int[] intArray2 = {100, 200, 300, 400, 500};

        System.out.println(intArray2);

        System.out.println(Arrays.toString(intArray2));

        System.out.println(Arrays.toString(stringArray));

//  Methods used in Arrays
//  sort()

        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));


//  Binary Search

        String searchTerm = "Ariana";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);

        System.out.println(binaryResult);

//  binarySearch searches the specified array of the given data type for the Arrays.sort() method prior to making this call. If it is not sorted, the results are undefined


//  Multidimensional Array

        String[][] classroom = new String[3][3];

//  first row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
//  second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
//  third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));

//  Operators - allows us to manipulate the values stored in the Identifiers
//  Arithmetic -> +, -, *, /, %
//  Comparison -> >, <. >=, <=. ==, !=
//  Assignment -> =, +=
//  Logical -> &&, ||, !

//  ArrayLists - resizeable arrays wherein elements can be added or removed whenever it is needed

//  Declaration

        ArrayList<String> students = new ArrayList<>();

        students.add("John");
        students.add("Paul");
        System.out.println(students);

//  To Access elements to an ArrayList - .get(index)
        System.out.println(students.get(0));


//  Update / Changing of element - .set(index, "Value")
        students.set(1, "George");
        System.out.println(students);

//  Removing elements - .remove(index)
        students.remove(1);
        System.out.println(students);

//  Removing all elements in an ArrayList - .clear()
        students.clear();
        System.out.println(students);


//  Getting the number of elements in an ArrayList using .size()
        System.out.println(students.size());


//  We can also declare and initialize a values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);



//  Hashmaps -> key:value pair

        HashMap<String, String> employeeRole = new HashMap<>();

//  Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

//  Retrieving field value
        System.out.println(employeeRole.get("Captain"));

//  Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

//  Retrieving HashMap keys / fields
        System.out.println(employeeRole.keySet());


//  with Integers as values
        HashMap<String, Integer> grades = new HashMap<>();
        grades.put("English", 89);
        grades.put("Math", 93);
        System.out.println(grades);

//  HashMap with ArrayLists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);

    }

}
