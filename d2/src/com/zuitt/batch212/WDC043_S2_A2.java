package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class WDC043_S2_A2 {

    public static void main(String[] args) {

        // ARRAY
        int[] intPrimeArray = new int[5];

        intPrimeArray[0] = 2;
        intPrimeArray[1] = 3;
        intPrimeArray[2] = 5;
        intPrimeArray[3] = 7;
        intPrimeArray[4] = 11;

        System.out.println("The first prime number is: " + intPrimeArray[0]);


        // ARRAYLIST
        ArrayList<String> friends = new ArrayList<>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);


        // HASHMAP
        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);




    }



}
